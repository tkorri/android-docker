FROM openjdk:8-jdk-slim

MAINTAINER Taneli Korri

WORKDIR /home/android

ENV SDK_TOOLS "4333796"
ENV ANDROID_HOME "/opt/android-sdk"
ENV PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools

# Install tools
RUN echo "Installing tools " && \
    apt-get update > /dev/null && \
    apt-get -y upgrade > /dev/null && \
    apt-get -y install wget git unzip rsync curl gnupg > /dev/null && \
    wget -q https://github.com/tkorri/acpublisher/releases/download/v1.0.1/acpublisher_1.0.1_Linux-64bit.deb && \
    wget -q https://github.com/tkorri/pspublisher/releases/download/v1.0.0/pspublisher_1.0.0_Linux-64bit.deb && \
    dpkg -i acpublisher*.deb pspublisher*.deb > /dev/null && \
    rm acpublisher*.deb pspublisher*.deb

# Install Android SDK Tools
RUN wget -q http://dl.google.com/android/repository/sdk-tools-linux-${SDK_TOOLS}.zip -O /tmp/sdk-tools.zip && \
    mkdir -p ${ANDROID_HOME} && \
    unzip -qq /tmp/sdk-tools.zip -d ${ANDROID_HOME} && \
    rm /tmp/sdk-tools.zip

# Install Android platforms & tools
RUN mkdir -p ~/.android/ && touch ~/.android/repositories.cfg && \
    echo "Approving licenses" && \
    yes | ${ANDROID_HOME}/tools/bin/sdkmanager "--licenses" > /dev/null && \
    echo "Updating SDKs" && \
    ${ANDROID_HOME}/tools/bin/sdkmanager "--update" && \
    echo "Installing platform tools" && \
    ${ANDROID_HOME}/tools/bin/sdkmanager "platform-tools" && \
    echo "Installing platforms" && \
    yes | ${ANDROID_HOME}/tools/bin/sdkmanager \
        "platforms;android-29" \
        "platforms;android-28" && \
    echo "Installing build tools" && \
    yes | ${ANDROID_HOME}/tools/bin/sdkmanager \
        "build-tools;29.0.3" \
        "build-tools;28.0.3"

# Install gcloud command line tool
# 1. Add the Cloud SDK distribution URI as a package source
# 2. Import the Google Cloud Platform public key
# 3. Update the package list and install the Cloud SDK
RUN echo "Installing gcloud cli tool " && \
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl -s -S https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - && \
    apt-get -y update > /dev/null && \
    apt-get -y install google-cloud-sdk > /dev/null
