# Android Docker build container

Small Docker container for building Android apps based on openjdk:8-jdk-slim.

## Content

- OpenJDK 8
- Android platform tools: latest
- Android platforms: android-29
- Android build tools: 29.0.2
- CLI tools: wget, git, unzip, rsync, [acpublisher](https://github.com/tkorri/acpublisher) 1.0.1, [pspublisher](https://github.com/tkorri/pspublisher) 1.0.0, [gcloud](https://cloud.google.com/sdk/gcloud)

